module.exports = function(grunt) {

    grunt.initConfig({
        babel: {
            options: {
                modules: 'amd'
            },
            dev: {
                options: {
                    sourceMap: true
                },
                files: [
                    {
                        expand: true,
                        cwd: 'lib/public/javascripts/src',
                        src: ['**/*.jsx'],
                        dest: 'lib/public/javascripts/out',
                        ext: '.js'
                    }
                ]
            },
            prod: {
                files: [
                    {
                        expand: true,
                        cwd: 'lib/public/javascripts/src',
                        src: ['**/*.jsx'],
                        dest: 'lib/public/javascripts/out',
                        ext: '.js'
                    }
                ]
            }
        },
        requirejs: {
            compile: {
                options: {
                    include: ['../bower_components/almond/almond'],
                    baseUrl: './lib/public/javascripts/out/',
                    paths: {
                        'underscore': '../bower_components/underscore/underscore-min',
                        'prism': '../prism',
                        'react': '../bower_components/react/react.min',
                        'page': '../bower_components/page/page'
                    },
                    shim: {
                        'underscore': {
                            'exports': '_'
                        },
                        'react': {
                            'exports': 'React'
                        },
                        'prism': {
                            'exports': 'Prism'
                        }
                    },
                    name: 'main',
                    out: './lib/public/javascripts/dist/main.js'
                }
            }
        },
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: 'lib/public/javascripts/src',
                        src: ['**/*.js'],
                        dest: 'lib/public/javascripts/out'
                    }
                ]
            }
        },
        watch: {
            options: {
                interrupt: true
            },
            src: {
                files: [
                    'lib/public/javascripts/src/**/*.jsx',
                    'lib/public/javascripts/src/**/*.js'
                ],
                tasks: ['copy', 'babel:dev']
            }
        }
    });

    // Modules
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-babel');

    // Register tasks
    grunt.registerTask('default', ['copy', 'babel:prod', 'requirejs']);
};
