var express = require('express');
var dbmanager = require('../dbmanager');
var io = require('../io');

var router = express.Router();
var messages = dbmanager.db.collection('messages');

var defaultRoute = function(req, res, next) {
  res.render('index', {
    env: process.env.NODE_ENV
  });

};

router.get('/', defaultRoute);
router.get('/edit', defaultRoute);

module.exports = router;
