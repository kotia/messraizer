import * as Prism from "prism";
import * as React from "react";
import * as utils from "utils";

const socket = io.connect(location.origin, {
  transports: ["polling", "websocket"]
});

class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: props.data };
    window.addEventListener('resize', this.setSize.bind(this));
  }
  componentDidMount() {
    this.el = React.findDOMNode(this);
    [this.state.width, this.state.height] = [this.el.clientWidth, this.el.clientHeight];

    Prism.highlightElement(this.el.querySelector("code"));
    this.setSize();
  }
  setSize() {
    this.el.style.fontSize = `${utils.screenHeight / this.state.height}em`;
  }
  render() {
    var data = this.state.data;
    return (
      <li className={`message ${this.props.active ? "active" : ""}`}>
        {data.message && <pre className="message__title">{data.message}</pre>}
        {data.imageUrl && <p className="image-container"><img className="img" src={data.imageUrl} /></p>}
        {data.code && <pre className={`code-block language-${data.codeType}`}>
          <code className={`code language-${data.codeType}`}>{data.code}</code>
        </pre>}
      </li>
    );
  }
}

class Messages extends React.Component {
  constructor(props) {
    super(props);
    var messages = props.data.filter((message) => !message.disabled);
    this.state = {
      messages: messages,
      activeElement: messages.length - 1
    };
    socket.on("message", this.handleAddMessage.bind(this));
    setInterval(this.showPreviousMessage.bind(this), 5000);
    window.addEventListener('keyup', this.handleKeyPress.bind(this));
  }
  showPreviousMessage() {
    --this.state.activeElement < 0 && (this.state.activeElement = this.state.messages.length - 1);
    this.setState(this.state);
  }
  showNextMessage() {
    ++this.state.activeElement > (this.state.messages.length - 1) && (this.state.activeElement = 0);
    this.setState(this.state);
  }
  handleAddMessage(data) {
    this.state.activeElement = this.state.messages.push(data.message) - 1;
    this.setState(this.state);
  }
  handleKeyPress(e) {
    if (e.keyCode === 37) {
      this.showPreviousMessage();
    } else if (e.keyCode === 39) {
      this.showNextMessage();
    }
  }
  render() {
    return (
      <div className="messages">
        <ul className="message-list">{this.state.messages.map((message, index) =>
          <Message key={message._id} data={message} active={index === this.state.activeElement} />
        )}</ul>
      </div>
    );
  }
}

socket.on("messages", function (data) {
  React.render(
    <Messages data={data.messages} />,
    document.getElementById("app")
  );
});
