require.config({
    "baseUrl": "/javascripts/out/",
    "paths": {
        "underscore": "/javascripts/bower_components/underscore/underscore",
        "prism": "/javascripts/prism",
        "react": "/javascripts/bower_components/react/react",
        "page": "/javascripts/bower_components/page/page"
    },
    "shim": {
        "underscore": {
            "exports": "_"
        },
        "react": {
            "exports": "React"
        },
        "prism": {
            "exports": "Prism"
        }
    }
});

require(["page"], function (page) {
    page('/', function() {
        require(["index"]);
    });
    page('/edit', function() {
        require(["editor"])
    });
    page();
});
