import * as Prism from "prism";
import * as React from "react";
import * as utils from "utils";

const socket = io.connect(location.origin, {
    transports: ["polling", "websocket"]
});

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {data: props.data};
    }

    componentDidMount() {
        this.el = React.findDOMNode(this);
        document.body.classList.add('editor');
        this.state.data.edited = false;
    }

    editData(e) {
        var field = e.target.dataset.field;
        this.state.data[field] = e.target.value;
        this.state.data.edited = true;
        this.setState(this.state);
    }

    changeDisablity() {
        this.props.changeDisablity(this.state.data._id);
    }

    removeMessage() {
        this.props.removeMessage(this.state.data._id);
    }

    edit() {
        socket.on('edit-success', function(){
            this.state.data.edited = false;
            this.setState(this.state);
        }.bind(this));

        socket.emit('edit', {
            message: this.state.data.message,
            code: this.state.data.code,
            codeType: this.state.data.codeType,
            imageUrl: this.state.data.imageUrl,
            disabled: this.state.data.disabled,
            timestamp: this.state.data.timestamp
        });
    }

    render() {
        var data = this.state.data, disableButton, editButton;

        if (data.disabled) {
            disableButton = <button onClick={this.changeDisablity.bind(this)}>enable</button>
        } else {
            disableButton = <button className="disable" onClick={this.changeDisablity.bind(this)}>disable</button>
        }

        if (data.edited) {
            editButton = <button onClick={this.edit.bind(this)}>edit</button>
        } else {
            editButton = <button className="inactive">edit</button>
        }

        return (
            <li className='li_edit'>
                Message:<br/>
                <textarea onChange={this.editData.bind(this)} data-field='message' value={data.message}/>
                <br/>
                Image URL:<br/>
                <input onChange={this.editData.bind(this)} data-field='imageUrl' type='text' value={data.imageUrl}/>
                <br/>
                Code Type:<br/>
                <select value={data.codeType} onChange={this.editData.bind(this)} data-field="codeType">
                    <option value="javascript">javascript</option>
                    <option value="java">java</option>
                    <option value="scala">scala</option>
                    <option value="css">css</option>
                    <option value="markup">markup</option>
                    <option value="clike">clike</option>
                </select>
                <br/>
                Code:<br/>
                <textarea onChange={this.editData.bind(this)} data-field='code' value={data.code}/>
                <br/>
                {editButton}
                {disableButton}
                <RemoveButton removeMessage={this.removeMessage.bind(this)}/>
            </li>
        );
    }
}

class Messages extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: props.data
        };
        socket.on("messages", function (data) {
            this.state.messages = data.messages;
            this.setState(this.state);
        }.bind(this));
    }

    changeDisablity(id) {
        socket.on("disable-success", function () {
            this.setState(this.state);
        }.bind(this));

        this.state.messages.forEach(function (message, key) {
            if (id === message._id) {
                message.disabled = !message.disabled;
                socket.emit("changeDisablity", {
                    timestamp: message.timestamp,
                    disabled: message.disabled
                });
            }
        }.bind(this));
    }

    removeMessage(id) {
        socket.on("delete-success", function () {
            this.setState(this.state);
        }.bind(this));

        this.state.messages.forEach(function (message, key) {
            if (id === message._id) {
                this.state.messages.splice(key, 1);
                socket.emit("remove", {
                    timestamp: message.timestamp
                });
            }
        }.bind(this));
    }

    addMessage() {
        var message = {
            message: 'New Message',
            code: '',
            codeType: 'javascript',
            imageUrl: '',
            disabled: false
        };
        socket.emit('add', message);
        socket.on('add-success', function () {
            socket.emit('getall');
        });
    }

    render() {
        return (
            <div className="messages">
                <ul className="message-list">{this.state.messages.map((message, index) =>
                        <Message removeMessage={this.removeMessage.bind(this)} changeDisablity={this.changeDisablity.bind(this)} key={message._id} data={message}/>
                )}</ul>
                <button onClick={this.addMessage.bind(this)} className="add-message">+</button>
            </div>
        );
    }
}

class RemoveButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {clicked: 0};
        this.setState(this.state);
    }
    addClick() {
        this.state.clicked++;
        this.setState(this.state);
    }
    render() {
        var removeButton;
        if (this.state.clicked < 5) {
            removeButton = <button className='remove-message' onClick={this.addClick.bind(this)}></button>
        } else if (this.state.clicked < 15) {
            removeButton = <button className='remove-message' onClick={this.addClick.bind(this)}>{15-this.state.clicked} times to click for removing</button>
        } else {
            removeButton = <button className='remove-message' onClick={this.props.removeMessage.bind(this)}>Delete message!</button>
        }
        return removeButton;
    }
}

socket.on("messages", function (data) {
    React.render(
        <Messages data={data.messages}/>,
        document.getElementById("app")
    );
});
